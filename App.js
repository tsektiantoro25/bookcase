import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';

import { 
  createStackNavigator,
  createAppContainer 
} from 'react-navigation'

// Import Component
import Home from './component/Home';
import Footer from './component/Footer';
import Category from './component/Category';
import About from './component/About';
import List from './component/List';
import Request from './component/Request';

// Kategori
import CatRakyat from './component/CatRakyat';
import CatFiksi from './component/CatFiksi';
import CatFabel from './component/CatFabel';

// Cerita
import DanauToba from './buku/DanauToba';
import Roro from './buku/Roro';
import Sangkuriang from './buku/Sangkuriang';
import TimunMas from './buku/TimunMas';

import Kelinci from './buku/Kelinci';
import Buaya from './buku/Buaya';
import SemutBelalang from './buku/SemutBelalang';


class Loading extends Component{
  render(){
    return <Text>Hallo Gaes</Text>
  }
} 


const Aplikasi = createStackNavigator({
  Home: {
    screen: Home, navigationOptions:{header:null}
  },
  Footer: {screen: Footer},
  Category: {screen: Category, navigationOptions:{title:"Kategori"}},
  About: {screen: About, navigationOptions:{title:" Tentang Kami"}},
  List: {screen: List, navigationOptions:{title:" Semua Buku"}},
  Request: {screen: Request, navigationOptions:{title:" Request Buku"}},

  // kategori
  CatRakyat: {screen: CatRakyat, navigationOptions:{title:"Kategori : Cerita Rakyat"}},
  CatFiksi: {screen: CatFiksi, navigationOptions:{title:"Kategori : Cerita Fiksi"}},
  CatFabel: {screen: CatFabel, navigationOptions:{title:"Kategori : Cerita Fabel"}},


  // Buku
  DanauToba: {screen: DanauToba, navigationOptions:{title: " DanauToba"}},
  Roro: {screen: Roro, navigationOptions:{title: " Roro Jongrang"}},
  Sangkuriang: {screen: Sangkuriang, navigationOptions:{title: " Sangkuriang"}},
  TimunMas: {screen: TimunMas, navigationOptions:{title: " Timun Mas"}},

  Kelinci: {screen: Kelinci, navigationOptions:{title: " Kelinci dan Siput"}},
  Buaya: {screen: Buaya, navigationOptions:{title: " Kisah Buaya yang Serakah"}},
  SemutBelalang: {screen: SemutBelalang, navigationOptions:{title: " Semut Dan Belalang"}},
}, {
  initialRouteName: 'Home'
})

const AppContainer = createAppContainer(Aplikasi);

export default AppContainer;
