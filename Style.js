'use strict';

var React = require('react-native');
import { Dimensions  } from 'react-native';
var screenWidth = Dimensions.get('window').width; //full screen width
var {
  StyleSheet,
} = React;

module.exports = StyleSheet.create({

	footer: {
		position: 'absolute',
		alignItems: 'center',
		justifyContent: 'center',
		bottom: 0,
		width: screenWidth,
	  	height: 60,
	  	backgroundColor:'#30b0ff'
	},
	// Background
	bgBiru: {
		flex: 1,
		backgroundColor: '#30b0ff',
		alignItems: 'center',
		justifyContent: 'center',
		paddingVertical:10
	},
	bgPutih: {
		flex: 1,
		backgroundColor: '#333',
		alignItems: 'center',
		justifyContent: 'center',
		paddingVertical:10
	},
	bgPutih2: {
		flex: 1,
		backgroundColor: '#333',
		padding:20,
		// justifyContent: 'center',
		paddingVertical:10
	},

	row: {
	    justifyContent: 'center',
	    paddingHorizontal: 10
	},
	form_group: {
	    justifyContent: 'center',
		paddingHorizontal: 10,
		marginTop:10
	},

	// Judul ================
	judul: {fontFamily:"JosefinSans-Bold", color:'white', fontSize:47, fontWeight:"bold"},

	// Button Style =========
	btnKuning: {
		alignItems: 'center',
		backgroundColor: '#f5e51b',
		padding: 10,
		borderRadius:3,
		width:230,
		marginTop:10
	},
	btnBiru: {
		alignItems: 'center',
		backgroundColor: '#30b0ff',
		padding: 10,
		borderRadius:3,
		width:230,
		marginTop:10
	},
	btnMerah: {
		alignItems: 'center',
		backgroundColor: '#f22613',
		padding: 10,
		borderRadius:3,
		width:230,
		marginTop:10
	},
	btnPutih: {
		alignItems: 'center',
		backgroundColor: '#fff',
		padding: 10,
		borderRadius:3,
		width:230,
		marginTop:10
	},

	// Warna Text =========
	textKuning: {
		color:'white',
		fontSize:18,
		fontWeight:'bold'
	},
	textPutih: {
		color:'white',
		fontSize:18,
		fontWeight:'bold',
		
	},
	textJosef:{
		color:'white',
		fontSize:18,
		fontWeight:'bold',
		fontFamily:"JosefinSans-Bold"
	},
	textPutihKecil: {
		color:'white',
		fontSize:15,
		// fontWeight:'bold',
		fontFamily:"JosefinSans-Bold"
	},
	textPutihH3: {
		color:'white',
		fontSize:16,fontFamily:"JosefinSans-Bold"
	},
	textPutihh4: {
		color:'white',
		fontSize:18,fontFamily:"JosefinSans-Bold"
	},
	textPutihH1: {
		color:'white',
		fontSize:20,
		fontWeight:'bold',
		fontFamily:"JosefinSans-Bold"
	},
	textHitamH1: {
		color:'#eee',
		fontSize:18,
		fontWeight:'bold',
		fontFamily:"JosefinSans-Bold"
	},
	textBiru: {
		color:'#30b0ff',
		fontSize:18,
		fontWeight:'bold'
	},
	textHitam: {
		color:'#eee',
		fontSize:14,
		fontWeight:'bold'
	},
	textBiruKecil: {
		color:'#30b0ff',
		fontSize:13,
		fontWeight:'bold',
		fontFamily:"JosefinSans-Bold"
	},
	textBiruTipis: {
		color:'#30b0ff',
		fontSize:18,
		// fontWeight:'bold'
	},
	textBold: {
		fontSize:17,
		fontWeight:'bold',
		fontFamily:"JosefinSans-Bold"
	},
	textBold2: {
		fontSize:15,
		fontWeight:'bold',
		fontFamily:"JosefinSans-Bold"
	},
	textBold3: {
		fontSize:14,
		// fontWeight:'bold',
		fontFamily:"josenfin-sans"
	},
	TextBold: {
		fontSize:20,
		fontWeight:'bold',
	},
	TextBoldKecil: {
		fontSize:17,
		fontWeight:'bold',
	},
	textTipis: {
		fontFamily:"JosefinSans-SemiBoldItalic",

	},
	textTipis2: {
		fontFamily:"JosefinSans-Bold",

	},
	textGambar: {
		fontFamily:"JosefinSans-Bold",
		marginBottom:10,
		fontWeight:"bold"
	},	
	// Jarak ========
	Atas: {
		marginTop:10
	},

	// Header
	headerBiru: {
		backgroundColor: '#30b0ff',
		padding: 20,
		alignItems:'center',
		justifyContent:'center'
	},
	Biru: {
		backgroundColor: '#30b0ff',
	},
	center:{
		alignItems:'center'
	},
	Kanan: {
		alignSelf: 'flex-end'
	},
	HeaderJudul: {
		color: 'white',
		fontSize:15,
		fontFamily: 'Cochin',
		marginTop:10
	},	
	// Inputan ==============
	Inputan: {
		width: 350,
		marginTop:20,
		fontSize:18
	},
	inputProfile:{
		marginTop:5,
		fontSize:16,
		borderBottomColor:'red',
		width:200
	},
	inputLokasi:{
		marginTop:5,
		fontSize:15,
		// borderBottomColor:'red',
		width:200
	},
	inputLogin: {
		width: 300,
		// marginTop:20,
		fontSize:16,
		color:'white',
		borderBottomColor:'white',
	},

	// FOOTER
	Footer: {
		width:'25%',
		alignItems:'center',
		justifyContent:'center'
	},

	// GARIS
	Garis: {
		borderBottomColor:'#333',
		borderBottomWidth: 1,
		paddingVertical:5
	},
	garisInput:{
		borderBottomColor:'#eee',borderBottomWidth: 1
	},
	GarisTipis: {
		borderBottomColor: '#eee',
		borderBottomWidth: 3,
		paddingVertical:5
	},
	GarisTebal: {
		borderBottomColor: '#eee',
		borderBottomWidth: 5,
		paddingVertical:5
	},
	Grid: {
		flexDirection: 'row',
		alignItems:'center',
		paddingVertical:0,
		paddingHorizontal:5
	},
	text: {
		fontSize:18
	},
	textKecil: {
		fontSize:14,color:'white'
	},
	logoSedang: {
		width:150,
		height:150
	},
	logoJudul: {
		width:320,
		height:150,
		marginTop:10
	},
	logoJudulLoading: {
		width:'100%',
		height:'100%'
	},
	logoKecil: {
		width:90,
		height:120
	},

	// GRID ==============
	g1: {
		width:'100%',
		padding:7
	},
	g2: {
		width:'20%',
		padding:7
	},
	g3: {
		width:'30%',
		padding:7
	},
	g4: {
		width:'40%',
		padding:7
	},
	g5: {
		width:'50%',
		padding:7
	},
	g6: {
		width:'60%',
		padding:7
	},
	g7: {
		width:'70%',
		padding:7
	},
	g8: {
		width:'80%',
		padding:7
	},
	g9: {
		width:'90%',
		padding:7
	},
	g10: {
		width:'10%',
		padding:7
	},
	labelMerah: {
		backgroundColor:'#c0392b',
		borderRadius:10,
		width:'30%',
		justifyContent:'center',
		alignItems:'center'
	},
	textLabel: {
		padding:5,
		fontSize:14,
		color:'white',
		fontWeight:'bold'
	}
});