import React, { Component } from 'react';

import { View, Text, ScrollView, Image, TouchableOpacity, Dimensions, Alert} from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";
var Style = require('./../Style');
var screenWidth = Dimensions.get('window').width; //full screen width

class Selanjutnya extends Component{
	render(){
		return <Text style={{color:'#f5e51b'}}>...Selengkapnya</Text>
	}
}

export default class Category extends Component{

	constructor(props){
		super(props);
		this.state = {
			cerita_rakyat: '5'
		}
	}	
	render(){
		
		return(
			<View style={Style.bgPutih2}>
				<ScrollView>

					<TouchableOpacity onPress={() => this.props.navigation.push('DanauToba')}>
						<View style={[Style.Grid, Style.GarisTipis]}>
							<View style={Style.g3}>
								<Image 
									source={require('./../img/danau_toba.jpg')}
									style={Style.logoKecil}
								/>
							</View>
							<View style={Style.g7}>
								<Text style={Style.textHitamH1}>Danau Toba</Text>
								<Text style={Style.textHitam}>Di wilayah Sumatera hiduplah seorang petani yang sangat rajin bekerja. Ia hidup sendiri sebatang kara. Setiap hari ia bekerja menggarap <Selanjutnya /></Text>
							</View>
						</View>
					</TouchableOpacity>

					<TouchableOpacity onPress={() => this.props.navigation.push('Roro')}>

						<View style={[Style.Grid, Style.GarisTipis]}>
							<View style={Style.g3}>
								<Image 
									source={require('./../img/roro.jpg')}
									style={Style.logoKecil}
								/>
							</View>
							<View style={Style.g7}>
								<Text style={Style.textHitamH1}>Roro Jongrang</Text>
								<Text style={Style.textHitam}>Alkisah, pada dahulu kala terdapat sebuah kerajaan besar yang bernama Prambanan. Rakyatnya hidup <Selanjutnya /></Text>
							</View>
						</View>
					</TouchableOpacity>

					<TouchableOpacity onPress={() => this.props.navigation.push('Sangkuriang')}>
						<View style={[Style.Grid, Style.GarisTipis]}>
							<View style={Style.g3}>
								<Image 
									source={require('./../img/sangkuriang.jpg')}
									style={Style.logoKecil}
								/>
							</View>
							<View style={Style.g7}>
								<Text style={Style.textHitamH1}>Sangkuriang</Text>
								<Text style={Style.textHitam}>Pada jaman dahulu, di Jawa Barat hiduplah seorang putri raja yang bernama Dayang Sumbi. Ia mempunyai <Selanjutnya />	</Text>
							</View>
						</View>
					</TouchableOpacity>

					<TouchableOpacity onPress={() => this.props.navigation.push('TimunMas')}>
						<View style={[Style.Grid, Style.GarisTipis]}>
							<View style={Style.g3}>
								<Image 
									source={require('./../img/timun.jpg')}
									style={Style.logoKecil}
								/>
							</View>
							<View style={Style.g7}>
								<Text style={Style.textHitamH1}>Timun Mas</Text>
								<Text style={Style.textHitam}>Pada zaman dahulu, hiduplah sepasang suami istri petani. Mereka tinggal di sebuah desa di dekat hutan <Selanjutnya/></Text>
							</View>
						</View>
					</TouchableOpacity>

				</ScrollView>

				
			</View>
		)
	}
}