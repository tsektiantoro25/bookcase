import React, { Component } from 'react';

import { View, Text, ScrollView, Image, TouchableOpacity, Dimensions, Alert} from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";
var Style = require('./../Style');
var screenWidth = Dimensions.get('window').width; //full screen width

export default class Category extends Component{

	constructor(props){
		super(props);
		this.state = {
			cerita_rakyat: '5'
		}
	}	
	render(){
		
		return(
			<View style={Style.bgPutih2}>
				<ScrollView>

					<TouchableOpacity onPress={() => this.props.navigation.navigate('CatRakyat')}>
						<View style={[Style.Grid, Style.GarisTipis]}>
							<View style={Style.g3}>
								<Image 
									source={require('./../img/cerita_rakyat.jpg')}
									style={Style.logoKecil}
								/>
							</View>
							<View style={Style.g7}>
								<Text style={Style.textHitamH1}>Cerita Rakyat</Text>
								<Text style={Style.textHitam}>Folklor meliputi legenda, musik, sejarah lisan, pepatah, lelucon, takhayul, dongeng, dan kebiasaan yang menjadi tradisi dalam suatu budaya, subkultur, atau kelompok</Text>
								<View style={[Style.labelMerah, Style.Atas]}>
									<Text style={Style.textLabel}>
										Total : 4
									</Text>
								</View>
							</View>
						</View>
					</TouchableOpacity>

					<TouchableOpacity onPress={() => this.props.navigation.navigate('CatFiksi')}>
						<View style={[Style.Grid, Style.GarisTipis]}>
							<View style={Style.g3}>
								<Image 
									source={require('./../img/cerita_fiksi.jpg')}
									style={Style.logoKecil}
								/>
							</View>
							<View style={Style.g7}>
								<Text style={Style.textHitamH1}>Cerita Fiksi</Text>
								<Text style={Style.textHitam}>karya sastra yang berisi cerita rekaan atau didasari dengan angan-angan (fantasi) dan bukan berdasarkan kejadian nyata, hanya berdasarkan imajinasi pengarang.</Text>
								<View style={[Style.labelMerah, Style.Atas]}>
									<Text style={Style.textLabel}>
										Total : 1
									</Text>
								</View>
							</View>
						</View>
					</TouchableOpacity>

					<TouchableOpacity>
						<View style={[Style.Grid, Style.GarisTipis]}>
							<View style={Style.g3}>
								<Image 
									source={require('./../img/cerita_dongeng.jpg')}
									style={Style.logoKecil}
								/>
							</View>
							<View style={Style.g7}>
								<Text style={Style.textHitamH1}>Cerita Dongeng</Text>
								<Text style={Style.textHitam}>merupakan bentuk sastra lama yang bercerita tentang suatu kejadian yang luar biasa yang penuh khayalan (fiksi) yang dianggap oleh masyarakat suatu hal yang tidak benar-benar terjadi.</Text>
								<View style={[Style.labelMerah, Style.Atas]}>
									<Text style={Style.textLabel}>
										Total : 0
									</Text>
								</View>
							</View>
						</View>
					</TouchableOpacity>

					<TouchableOpacity onPress={() => this.props.navigation.navigate('CatFabel')}>
						<View style={[Style.Grid, Style.GarisTipis]}>
							<View style={Style.g3}>
								<Image 
									source={require('./../img/cerita_fabel.jpg')}
									style={Style.logoKecil}
								/>
							</View>
							<View style={Style.g7}>
								<Text style={Style.textHitamH1}>Cerita Fabel</Text>
								<Text style={Style.textHitam}>cerita yang menceritakan kehidupan hewan yang berperilaku menyerupai manusia</Text>
								<View style={[Style.labelMerah, Style.Atas]}>
									<Text style={Style.textLabel}>
										Total : 3
									</Text>
								</View>
							</View>
						</View>
					</TouchableOpacity>

					<TouchableOpacity>
						<View style={[Style.Grid, Style.GarisTipis]}>
							<View style={Style.g3}>
								<Image 
									source={require('./../img/cerita_parabel.jpg')}
									style={Style.logoKecil}
								/>
							</View>
							<View style={Style.g7}>
								<Text style={Style.textHitamH1}>Cerita Parabel</Text>
								<Text style={Style.textHitam}>cerita rekaan untuk menyampaikan ajaran agama, moral, atau kebenaran umum dengan menggunakan perbandingan atau ibarat</Text>
								<View style={[Style.labelMerah, Style.Atas]}>
									<Text style={Style.textLabel}>
										Total : 0
									</Text>
								</View>
							</View>
						</View>
					</TouchableOpacity>

					<TouchableOpacity
						onPress={() => Alert.alert('Update !', 'Tunggu 2 Tahun Lagi')}
					>
						<View style={[Style.Grid, {marginBottom:70, alignItems:'center', justifyContent:'center'}]}>
							<Text style={Style.textHitam}>Updated...</Text>
						</View>
					</TouchableOpacity>

				</ScrollView>

				<View style={Style.footer}>
						<View style={Style.Grid}>

							<View style={[Style.center,{width:'20%',padding:5}]}>
								<TouchableOpacity
									onPress={() => this.props.navigation.push('Category')}
								>
									<Icon 
										name="md-list-box"
										color="white"
										size={30}
									/>
								</TouchableOpacity>
							</View>
							<View style={[Style.center,{width:'20%',padding:5}]}>
								<TouchableOpacity
									onPress={() => this.props.navigation.push('Request')}
								>
									<Icon 
										name="md-apps"
										color="white"
										size={30}
									/>
								</TouchableOpacity>
							</View>
							<View style={[Style.center,{width:'20%',padding:5}]}>
								<TouchableOpacity
									onPress={() => this.props.navigation.push('Home')}
								>
									<Icon 
										name="md-home"
										color="white"
										size={30}
									/>
								</TouchableOpacity>
							</View>
							<View style={[Style.center,{width:'20%',padding:5}]}>
								<TouchableOpacity
									onPress={() => this.props.navigation.push('List')}
								>
									<Icon 
										name="md-list"
										color="white"
										size={30}
									/>
								</TouchableOpacity>
							</View>
							<View style={[Style.center,{width:'20%',padding:5}]}>
								<TouchableOpacity
									onPress={() => this.props.navigation.push('About')}
								>
									<Icon 
										name="md-help"
										color="white"
										size={30}
									/>
								</TouchableOpacity>
							</View>
						</View>

						<View style={Style.Grid}>
							<View style={[Style.center,{width:'20%'}]}>
								<Text style={Style.textKecil}>Category</Text>
							</View>
							<View style={[Style.center,{width:'20%'}]}>
								<Text style={Style.textKecil}>Request</Text>
							</View>
							<View style={[Style.center,{width:'20%'}]}>
								<Text style={Style.textKecil}>Home</Text>
							</View>
							<View style={[Style.center,{width:'20%'}]}>
								<Text style={Style.textKecil}>List</Text>
							</View>
							<View style={[Style.center,{width:'20%'}]}>
								<Text style={Style.textKecil}>About</Text>
							</View>
						</View>
					</View>
			</View>
		)
	}
}