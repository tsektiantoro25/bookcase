import React, { Component } from 'react';
import { View , Text, Dimensions, TouchableOpacity, Image } from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";
// import { Button } from 'react-native-elements'

// import Footer from './Footer';

// style
var Style = require('./../Style');
var screenWidth = Dimensions.get('window').width; //full screen width
class Home extends Component{

	constructor(props){
		super(props);
		this.state = {
			loading: false
		}
	}

	componentDidMount(){
		setTimeout(() => this.setState({loading: true}) ,4000)
	}

	render(){

		if (this.state.loading) {
			// Masuk Ke Home
			return(
				<View style={Style.bgPutih}>
					<Text style={Style.textBiru}>Selamat Datang Di Aplikasi BookCase</Text>
					<View style={{marginTop:30}} />
					<TouchableOpacity style={Style.btnMerah} onPress={() => this.props.navigation.navigate('List')}>
						<Text style={Style.textHitam}>Cari Cepat</Text>
					</TouchableOpacity>
					<View style={Style.footer}>
						<View style={Style.Grid}>

							<View style={[Style.center,{width:'20%',padding:5}]}>
								<TouchableOpacity
									onPress={() => this.props.navigation.push('Category')}
								>
									<Icon 
										name="md-list-box"
										color="white"
										size={30}
									/>
								</TouchableOpacity>
							</View>
							<View style={[Style.center,{width:'20%',padding:5}]}>
								<TouchableOpacity
									onPress={() => this.props.navigation.push('Request')}
								>
									<Icon 
										name="md-apps"
										color="white"
										size={30}
									/>
								</TouchableOpacity>
							</View>
							<View style={[Style.center,{width:'20%',padding:5}]}>
								<TouchableOpacity
									onPress={() => this.props.navigation.push('Home')}
								>
									<Icon 
										name="md-home"
										color="white"
										size={30}
									/>
								</TouchableOpacity>
							</View>
							<View style={[Style.center,{width:'20%',padding:5}]}>
								<TouchableOpacity
									onPress={() => this.props.navigation.push('List')}
								>
									<Icon 
										name="md-list"
										color="white"
										size={30}
									/>
								</TouchableOpacity>
							</View>
							<View style={[Style.center,{width:'20%',padding:5}]}>
								<TouchableOpacity
									onPress={() => this.props.navigation.push('About')}
								>
									<Icon 
										name="md-help"
										color="white"
										size={30}
									/>
								</TouchableOpacity>
							</View>
						</View>

						<View style={Style.Grid}>
							<View style={[Style.center,{width:'20%'}]}>
								<Text style={Style.textKecil}>Category</Text>
							</View>
							<View style={[Style.center,{width:'20%'}]}>
								<Text style={Style.textKecil}>Request</Text>
							</View>
							<View style={[Style.center,{width:'20%'}]}>
								<Text style={Style.textKecil}>Home</Text>
							</View>
							<View style={[Style.center,{width:'20%'}]}>
								<Text style={Style.textKecil}>List</Text>
							</View>
							<View style={[Style.center,{width:'20%'}]}>
								<Text style={Style.textKecil}>About</Text>
							</View>
						</View>
					</View>
				</View>
			);
		}else{
			return(
				<View style={Style.bgPutih}>
					<View style={{alignItems:"center"}}>
						<View style={Style.row}>
							<Image 
								source={require('./../logos.png')}
								style={{alignSelf: 'stretch'}}
							/>
						</View>
						<View style={Style.row}>
							<Text style={Style.judul}>BookCase</Text>
						</View>
					</View>
				</View>
			);
		}
	}
}

export default Home;