import React, { Component } from 'react';
import { View, StyleSheet, Text, Linking, TextInput,TouchableOpacity,Image} from 'react-native';

// import Footer from './Footer';
	
// style
var Style = require('./../Style');

export default class About extends Component{

		constructor(props) {
			super(props);
			this.state = {
			  mobile_no: '',
			};
			}
			
		  render() {
			return (
				
			  <View style={styles.container}>
				<TextInput
				  value={this.state.mobile_no}
				  onChangeText={mobile_no => this.setState({ mobile_no })}
				/>
				<View style={[Style.center, {marginBottom:25}]}>
								<Image
									style={[Style.logoSedang, {marginBottom:5, marginTop:30}]}
									source={require('./../img/logo2.png')}
								/>
							</View>
				<Text style={{fontWeight: 'bold',fontSize: 25, color:'white'}}>
					Ingin Request buku ?
				 </Text>
				 <TouchableOpacity style={Style.btnMerah} onPress={() => {
					Linking.openURL(
					  'http://api.whatsapp.com/send?text=Halo Admin BookCase Saya Ingin Request Buku&phone=6282138355492' + this.state.mobile_no
					);
				  }}>
									<Text style={Style.textPutih}>Klik Disini</Text>
								</TouchableOpacity>
								
				<View style={[Style.Atas, Style.center,Style.GarisTipis,{paddingVertical:100}]}>
									<Text style={Style.textHitamH1}>
										- Angkatan 25 -
									</Text>
									<Text style={Style.textHitamH1}>
										Telkom Schools Purwokerto
									</Text>
									
								</View>	
								<View style={Style.Garis}/>
				</View>
				);
			}
		  }
		   
		  const styles = StyleSheet.create({
			container: {
			  flex: 1,
			  alignItems: 'center',
			  paddingTop: 30,
			  backgroundColor: '#333',
		   },
		  });	