import React, { Component } from 'react';
import { View , Text, Dimensions, TouchableOpacity, Image } from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";

// import Footer from './Footer';
	
// style
var Style = require('./../Style');
var screenWidth = Dimensions.get('window').width; //full screen width

export default class About extends Component{

	constructor(props){
		super(props);
		this.state = {
			a1: '',
			a2: '',
			a3: '',
			a4: '',
		}
	}

	Klik = () => {
		this.setState({a1: '- Ana Yuni Z. (05)'});
		this.setState({a2: '- Huwaida Nisrina. (14)'});
		this.setState({a3: '- Muhammad Ilham S. (23)'});
		this.setState({a4: '- Tri Aji Sektiantoro. (32)'});
	}

	render(){
		return(
				<View style={{backgroundColor:'#333'}}>
					<View style={[Style.row, {marginBottom:40}]}>
						<View style={[Style.center, {marginBottom:25}]}>
							<Image
								style={[Style.	logoSedang, {marginBottom:5, marginTop:30}]}
								source={require('./../img/logo.png')}
							/>
						</View>
						<View style={[Style.row]}>
							<Text style={Style.textBiru}> BookCase </Text>
							<Text style={Style.textHitam}>adalah Aplikasi Buku / Virtual Book,</Text>
							<Text style={Style.textHitam}>yang disusun sesederhana mungkin. Aplikasi ini cocok</Text> 
							<Text style={Style.textHitam}>sekali untuk Anak di bawah Usia</Text>
							<Text style={Style.textHitam}>Karena Dari Cerita yang sangat Menarik dan </Text>
							<Text style={Style.textHitam}>Mempunyai Banyak Makna dan Pesan Moral Tersendiri</Text>
							<View style={Style.Atas} />
							<View style={[Style.center, Style.Atas]}>
								<TouchableOpacity style={Style.btnMerah} onPress={() => this.Klik()}>
									<Text style={Style.textPutih}>SIAPA KAMI ?</Text>
								</TouchableOpacity>
							</View>

							<View style={[Style.Atas, Style.GarisTipis, {paddingVertical:20}]}>
								<Text style={Style.textHitam}>
									{this.state.a1}
								</Text>
								<Text style={Style.textHitam}>
									{this.state.a2}
								</Text>
								<Text style={Style.textHitam}>
									{this.state.a3}
								</Text>
								<Text style={Style.textHitam}>
									{this.state.a4}
								</Text>
							</View>
							<View style={[Style.Atas, Style.center]}>
								<Text style={Style.textHitamH1}>
									Telkom Schools Purwokerto
								</Text>
							</View>

						</View>	
					</View>

					<View style={Style.Garis}/>

					
				</View>
			);
	}
}