import React, { Component } from 'react';
import { View , Text, Dimensions, TouchableOpacity } from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";
// style
var Style = require('./../Style');
var screenWidth = Dimensions.get('window').width; //full screen width
class Footer extends Component{

	render(){		
		// Masuk Ke Home
		return(
			<View style={Style.footer}>
				<View style={Style.Grid}>

					<View style={[Style.center,{width:'20%',padding:5}]}>
						<TouchableOpacity
							onPress={() => this.props.navigation.push('Category')}
						>
							<Icon 
								name="md-list-box"
								color="white"
								size={30}
							/>
						</TouchableOpacity>
					</View>
					<View style={[Style.center,{width:'20%',padding:5}]}>
						<TouchableOpacity
							onPress={() => this.props.navigation.push('Category')}
						>
							<Icon 
								name="md-apps"
								color="white"
								size={30}
							/>
						</TouchableOpacity>
					</View>
					<View style={[Style.center,{width:'20%',padding:5}]}>
						<TouchableOpacity
							onPress={() => this.props.navigation.push('Home')}
						>
							<Icon 
								name="md-home"
								color="white"
								size={30}
							/>
						</TouchableOpacity>
					</View>
					<View style={[Style.center,{width:'20%',padding:5}]}>
						<TouchableOpacity
							onPress={() => this.props.navigation.push('List')}
						>
							<Icon 
								name="md-list"
								color="white"
								size={30}
							/>
						</TouchableOpacity>
					</View>
					<View style={[Style.center,{width:'20%',padding:5}]}>
						<TouchableOpacity
							onPress={() => this.props.navigation.push('About')}
						>
							<Icon 
								name="md-help"
								color="white"
								size={30}
							/>
						</TouchableOpacity>
					</View>
				</View>
			</View>
		);
	}
}

export default Footer;