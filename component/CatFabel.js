import React, { Component } from 'react';

import { View, Text, ScrollView, Image, TouchableOpacity, Dimensions, Alert} from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";
var Style = require('./../Style');
var screenWidth = Dimensions.get('window').width; //full screen width

class Selanjutnya extends Component{
	render(){
		return <Text style={{color:'#f5e51b'}}>...Selengkapnya</Text>
	}
}

export default class CatFabel extends Component{

	render(){
		
		return(
			<View style={Style.bgPutih2}>
				<ScrollView>

					<TouchableOpacity onPress={() => this.props.navigation.push('Kelinci')}>
						<View style={[Style.Grid, Style.GarisTipis]}>
							<View style={Style.g3}>
								<Image 
									source={require('./../img/kelinci_siput.jpg')}
									style={Style.logoKecil}
								/>
							</View>
							<View style={Style.g7}>
								<Text style={Style.textHitamH1}>Kelinci dan Siput</Text>
								<Text style={Style.textHitam}>Pada jaman dahulu hiduplah dua binatang dihutan yang luas. Binatang itu ialah kelinci dan siput <Selanjutnya /></Text>
							</View>
						</View>
					</TouchableOpacity>

					<TouchableOpacity onPress={() => this.props.navigation.push('Buaya')}>
						<View style={[Style.Grid, Style.GarisTipis]}>
							<View style={Style.g3}>
								<Image 
									source={require('./../img/cerita-dongeng-buaya-yang-serakah.jpg')}
									style={Style.logoKecil}
								/>
							</View>
							<View style={Style.g7}>
								<Text style={Style.textHitamH1}>Kisah Buaya Yang Serakah</Text>
								<Text style={Style.textHitam}>Di pinggiran sungai ada seekor buaya yang sedang kelaparan, sudah tiga hari Buaya itu belum makan perutnya terasa la <Selanjutnya /></Text>
							</View>
						</View>
					</TouchableOpacity>

					<TouchableOpacity onPress={() => this.props.navigation.push('SemutBelalang')}>
						<View style={[Style.Grid, Style.GarisTipis]}>
							<View style={Style.g3}>
								<Image 
									source={require('./../img/cerita-hewan-semut-dan-belalang.jpg')}
									style={Style.logoKecil}
								/>
							</View>
							<View style={Style.g7}>
								<Text style={Style.textHitamH1}>Semut dan Belalang</Text>
								<Text style={Style.textHitam}>Di musim panas yang hangat dan cerah sedikit menggoda Belalang untuk memainkan biola kesayangan sambil bernyanyi <Selanjutnya /></Text>
							</View>
						</View>
					</TouchableOpacity>

				</ScrollView>

				
			</View>
		)
	}
}