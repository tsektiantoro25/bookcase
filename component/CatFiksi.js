import React, { Component } from 'react';

import { View, Text, ScrollView, Image, TouchableOpacity, Dimensions, Alert} from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";
var Style = require('./../Style');
var screenWidth = Dimensions.get('window').width; //full screen width

class Selanjutnya extends Component{
	render(){
		return <Text style={{color:'#f5e51b'}}>...Selengkapnya</Text>
	}
}

export default class CatFiksi extends Component{

	render(){
		
		return(
			<View style={Style.bgPutih2}>
				<ScrollView>

					<TouchableOpacity onPress={() => this.props.navigation.push('DanauToba')}>
						<View style={[Style.Grid, Style.GarisTipis]}>
							<View style={Style.g3}>
								<Image 
									source={require('./../img/danau_toba.jpg')}
									style={Style.logoKecil}
								/>
							</View>
							<View style={Style.g7}>
								<Text style={Style.textHitamH1}>Danau Toba</Text>
								<Text style={Style.textHitam}>Di wilayah Sumatera hiduplah seorang petani yang sangat rajin bekerja. Ia hidup sendiri sebatang kara. Setiap hari ia bekerja menggarap <Selanjutnya /></Text>
							</View>
						</View>
					</TouchableOpacity>

				</ScrollView>

				
			</View>
		)
	}
}