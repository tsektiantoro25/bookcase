import React, { Component } from 'react';

import { View, Text, ScrollView, Image, TouchableOpacity, Dimensions, Alert} from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";

var Style = require('./../Style');

export default class TimunMas extends Component{

	render(){
		
		return(
			<View style={Style.bgPutih2}>
				<ScrollView>
					<View style={[Style.row, Style.center]}>

						<Image 
							source={require('./../img/timun1.jpg')}
							style={Style.logoJudul}
						/>
						<View style={Style.garisTipis}/>
						<Text style={[Style.Atas, Style.textPutihKecil]}>Pada zaman dahulu, hiduplah sepasang suami istri petani. Mereka tinggal di sebuah desa di dekat hutan. Mereka hidup bahagia. Sayangnya mereka belum saja dikaruniai seorang anak pun.

Setiap hari mereka berdoa pada Yang Maha Kuasa. Mereka berdoa agar segera diberi seorang anak. Suatu hari seorang raksasa melewati tempat tinggal mereka. Raksasa itu mendengar doa suami istri itu. Raksasa itu kemudian memberi mereka biji mentimun.</Text>
						
						<Image source={require('./../img/timun3.jpg')} style={Style.logoJudul} />

						<Text style={[Style.Atas, Style.textPutihKecil]}>“Tanamlah biji ini. Nanti kau akan mendapatkan seorang anak perempuan,” kata Raksasa. “Terima kasih, Raksasa,” kata suami istri itu. “Tapi ada syaratnya. Pada usia 17 tahun anak itu harus kalian serahkan padaku,” sahut Raksasa. Suami istri itu sangat merindukan seorang anak. Karena itu tanpa berpikir panjang mereka setuju.

Suami istri petani itu kemudian menanam biji-biji mentimun itu. Setiap hari mereka merawat tanaman yang mulai tumbuh itu dengan sebaik mungkin. Berbulan-bulan kemudian tumbuhlah sebuah mentimun berwarna keemasan.</Text>

						<Text style={[Style.Atas, Style.textPutihKecil]}>Buah mentimun itu semakin lama semakin besar dan berat. Ketika buah itu masak, mereka memetiknya. Dengan hati-hati mereka memotong buah itu. Betapa terkejutnya mereka, di dalam buah itu mereka menemukan bayi perempuan yang sangat cantik. Suami istri itu sangat bahagia. Mereka memberi nama bayi itu Timun Mas.

Tahun demi tahun berlalu. Timun Mas tumbuh menjadi gadis yang cantik. Kedua orang tuanya sangat bangga padanya. Tapi mereka menjadi sangat takut. Karena pada ulang tahun Timun Mas yang ke-17, sang raksasa datang kembali. Raksasa itu menangih janji untuk mengambil Timun Mas.</Text>
						<Image 
							source={require('./../img/timun4.jpg')}
							style={Style.logoJudul}
						/>
						<Text style={[Style.Atas, Style.textPutihKecil]}>Petani itu mencoba tenang. “Tunggulah sebentar. Timun Mas sedang bermain. Istriku akan memanggilnya,” katanya. Petani itu segera menemui anaknya. “Anakkku, ambillah ini,” katanya sambil menyerahkan sebuah kantung kain. “Ini akan menolongmu melawan Raksasa. Sekarang larilah secepat mungkin,” katanya. Maka Timun Mas pun segera melarikan diri.

Suami istri itu sedih atas kepergian Timun Mas. Tapi mereka tidak rela kalau anaknya menjadi santapan Raksasa. Raksasa menunggu cukup lama. Ia menjadi tak sabar. Ia tahu, telah dibohongi suami istri itu. Lalu ia pun menghancurkan pondok petani itu. Lalu ia mengejar Timun Mas ke hutan.</Text>
						<Image 
							source={require('./../img/timun5.jpg')}
							style={Style.logoJudul}
						/>
						<Text style={[Style.Atas, Style.textPutihKecil]}>Raksasa segera berlari mengejar Timun Mas. Raksasa semakin dekat. Timun Mas segera mengambil segenggam garam dari kantung kainnya. Lalu garam itu ditaburkan ke arah Raksasa. Tiba-tiba sebuah laut yang luas pun terhampar. Raksasa terpaksa berenang dengan susah payah.

Timun Mas berlari lagi. Tapi kemudian Raksasa hampir berhasil menyusulnya. Timun Mas kembali mengambil benda ajaib dari kantungnya. Ia mengambil segenggam cabai. Cabai itu dilemparnya ke arah raksasa. Seketika pohon dengan ranting dan duri yang tajam memerangkap Raksasa. Raksasa berteriak kesakitan. Sementara Timun Mas berlari menyelamatkan diri.</Text>

						<Text style={[Style.Atas, Style.textPutihKecil]}>Tapi Raksasa sungguh kuat. Ia lagi-lagi hampir menangkap Timun Mas. Maka Timun Mas pun mengeluarkan benda ajaib ketiga. Ia menebarkan biji-biji mentimun ajaib. Seketika tumbuhlah kebun mentimun yang sangat luas. Raksasa sangat letih dan kelaparan. Ia pun makan mentimun-mentimun yang segar itu dengan lahap. Karena terlalu banyak makan, Raksasa tertidur.

Timun Mas kembali melarikan diri. Ia berlari sekuat tenaga. Tapi lama kelamaan tenaganya habis. Lebih celaka lagi karena Raksasa terbangun dari tidurnya. Raksasa lagi-lagi hampir menangkapnya. Timun Mas sangat ketakutan. Ia pun melemparkan senjatanya yang terakhir, segenggam terasi udang. Lagi-lagi terjadi keajaiban. Sebuah danau lumpur yang luas terhampar. Raksasa terjerembab ke dalamnya. Tangannya hampir menggapai Timun Mas. Tapi danau lumpur itu menariknya ke dasar. Raksasa panik. Ia tak bisa bernapas, lalu tenggelam.</Text>
						<Image 
							source={require('./../img/timun6.jpg')}
							style={Style.logoJudul}
						/>
						
						<Text style={[Style.Atas, Style.textPutihKecil]}>Timun Mas lega. Ia telah selamat. Timun Mas pun kembali ke rumah orang tuanya. Ayah dan Ibu Timun Mas senang sekali melihat Timun Mas selamat. Mereka menyambutnya. “Terima Kasih, Tuhan. Kau telah menyelamatkan anakku,” kata mereka gembira.</Text>

						<Text style={[Style.Atas, Style.textPutihKecil]}>Sejak saat itu Timun Mas dapat hidup tenang bersama orang tuanya. Mereka dapat hidup bahagia tanpa ketakutan lagi.</Text>


					</View>
				</ScrollView>
			</View>
		)
	}
}