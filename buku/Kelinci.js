import React, { Component } from 'react';

import { View, Text, ScrollView, Image, TouchableOpacity, Dimensions, Alert} from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";

var Style = require('./../Style');

export default class Kelinci extends Component{

	render(){
		
		return(
			<View style={Style.bgPutih2}>
				<ScrollView>
					<View style={[Style.row, Style.center]}>

						<Image 
							source={require('./../img/kelinci_siput.jpg')}
							style={Style.logoJudul}
						/>
						<View style={Style.garisTipis}/>
						<Text style={[Style.Atas, Style.textPutihKecil]}>
							Pada jaman dahulu hiduplah dua binatang dihutan yang luas. Binatang itu ialah kelinci dan siput. Kelinci tersebut memiliki sifat sangat sombong dan pemarah. Bahkan sang kelinci sering meremehkan hewan hewan lainnya. Ketika ia berjalan jalan disekitar hutan, kelinci itu bertemu sang siput berjalan dengan lambatnya. Kelinci berkata, “ Siput, apa yang kamu lakukan disini?” Siput menjawab,” Aku sedang mencari penghidupan.” Kelinci tersebut malah marah karena ia berpikir sang siput hanya berlagak mencari penghidupan. Si siput berusaha menjelaskan maksud jawabannya tadi namun kelinci tetap saja marah bahkan ia juga mengancam akan menginjak tubuh siput.
						</Text>
						
						<Image source={require('./../img/kelinci2.jpg')} style={Style.logoJudul} />

						<Text style={[Style.Atas, Style.textPutihKecil]}>
							Akhirnya siput menantang adu kecepatan dengan kelinci. Mendengar tantangan tersebut sang kelinci marah besar. Ia menerima tawaran siput dan berkata dengan keras agar hewan hewan lain menjadi saksi perlombaan lari antara kelinci dengan siput. Hari perlombaan tiba, kelinci dan siput tadi berlomba lari untuk sampai kefinish. Namun sebelumnya si siput memiliki akal untuk meminta siput siput lainnya berada di titik titik jalur lomba lari sampai ke finish. Hal ini dikarenakan cangkang semua siput memiiliki kesamaan, dengan begitu hewan hewan lain tidak akan curiga. Kelincipun melompat dan berlari meninggalkan siput dijalur start. Akhirnya rencana siput berjalan lancar dan akhirnya siput tadi menjadi pemenang walaupun sebenarnya yang memasuki finish ialah temannya. Dengan kemenangan siput membuat kelinci menjadi tidak sombong dan tidak pemarah lagi.
						</Text>
						<Image 
							source={require('./../img/kelinci3.jpg')}
							style={Style.logoJudul}
						/>

					</View>
				</ScrollView>
			</View>
		)
	}
}