import React, { Component } from 'react';

import { View, Text, ScrollView, Image, TouchableOpacity, Dimensions, Alert} from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";

var Style = require('./../Style');

export default class SemutBelalang extends Component{

	render(){
		
		return(
			<View style={Style.bgPutih2}>
				<ScrollView>
					<View style={[Style.row, Style.center]}>

						<Image 
							source={require('./../img/cerita-hewan-semut-dan-belalang.jpg')}
							style={Style.logoJudul}
						/>
						<View style={Style.garisTipis}/>
						<Text style={[Style.Atas, Style.textPutihKecil]}>
							Di musim panas yang hangat dan cerah sedikit menggoda Belalang untuk memainkan biola kesayangan sambil bernyanyi dan menari. Hampir setiap harinya itulah yang dilakukan belalang. Ia tidak terpikir untuk melakukan aktifitas lainnya seperti bekerja atau bersiap untuk mengumpulkan bekal musim dingin.
						</Text>
						
						<Image source={require('./../img/belalang1.jpg')} style={Style.logoJudul} />

						<Text style={[Style.Atas, Style.textPutihKecil]}>
							Sedikit pun tidak pernah terlintas dalam benak belalang bahwa musim panas yang sedang dinikmatinya sekarang sudah akan berakhir. Musim panas yang membuatnya ceria sudah akan berganti ke musim dingin, dimana hujan akan turun dengan lebat disertai suhu udara yang sangat rendah.
						</Text>

						<Text style={[Style.Atas, Style.textPutihKecil]}>
							Disaat belalang sedang asiknya bermain biola, dia melihat semut yang sedang giat melewati rumahnya. Belalang yang masih riang tersebut ingin mengajak semut bermain bersama dan semut pun diundangnya untuk bersenang-senang ke kediaman belalang.
						</Text>
						<Image 
							source={require('./../img/belalang2.jpg')}
							style={Style.logoJudul}
						/>
						<Text style={[Style.Atas, Style.textPutihKecil]}>

						Tak disangka belalang ternyata semut menolak undangan belalang dengan santun, semut berkata pada belalang,
“Maaf Belalang, aku masih ingin bekerja untuk bekal di musim dingin. Aku harus mengumpulkan cadangan makanan yang banyak serta memperbaiki tempat tinggal agar lebih hangat.”</Text>
						<Image 
							source={require('./../img/belalang3.jpg')}
							style={Style.logoJudul}
						/>
						<Text style={[Style.Atas, Style.textPutihKecil]}>
							“Berhentilah memikirkan hal yang tidak penting semut, mari kita bernyanyi dan bersenang-senang, ayolah nikmati hidup kita”, Sanggah belalang. Belalang pun masih dengan kebiasaannya untuk bersenang-senang tanpa memikirkan apapun.

Tidak disangka musim panas berakhir jauh lebih cepat dari pada biasanya. Belalang yang terbiasa gembira lantas panik bukan main. Ia tidak memiliki persediaan makanan yang cukup ditambah rumahnya yang rusak dan tidak layak huni karena diterjang badai.
						</Text>

						<Text style={[Style.Atas, Style.textPutihKecil]}>
							Dengan harapan tinggi dan lunglai belalang menuju rumah semut dan meminta bantuan untuk diperbolehkan tinggal bersama dan meminta makan. Mendengar permohonan tersebut semut menjawab, “Maafkan aku belalang aku tidak bisa membantumu, rumahku terlalu sempit untukmu, dan bekalku hanya cukup untuk keluargaku saja”.
Belalang akhirnya pun meninggalkan rumah semut dengan rasa menyesal dan sedih. Dalam hati ia bergumam, “Andai saja aku mengikuti nasihat semut saat itu untuk bekerja keras, pasti saat ini aku bisa kenyang dan tidur nyenyak di dalam rumah”.

Tamat.
						</Text>
						<Image 
							source={require('./../img/belalang4.jpg')}
							style={Style.logoJudul}
						/>
						

					</View>
				</ScrollView>
			</View>
		)
	}
}