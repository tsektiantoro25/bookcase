import React, { Component } from 'react';

import { View, Text, ScrollView, Image, TouchableOpacity, Dimensions, Alert} from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";

var Style = require('./../Style');

export default class Roro extends Component{

	render(){
		
		return(
			<View style={Style.bgPutih2}>
				<ScrollView>
					<View style={[Style.row, Style.center]}>

						<Image 
							source={require('./../img/roro.jpg')}
							style={Style.logoJudul}
						/>
						<View style={Style.garisTipis}/>
						<Text style={[Style.Atas, Style.textPutihKecil]}>Alkisah, pada dahulu kala terdapat sebuah kerajaan besar yang bernama Prambanan. Rakyatnya hidup tenteran dan damai. Tetapi, apa yang terjadi kemudian? Kerajaan Prambanan diserang dan dijajah oleh negeri Pengging. Ketentraman Kerajaan Prambanan menjadi terusik. Para tentara tidak mampu menghadapi serangan pasukan Pengging. Akhirnya, kerajaan Prambanan dikuasai oleh Pengging, dan dipimpin oleh Bandung Bondowoso.</Text>
						
						<Image source={require('./../img/roro3.jpg')} style={Style.logoJudul} />

						<Text style={[Style.Atas, Style.textPutihKecil]}>Bandung Bondowoso seorang yang suka memerintah dengan kejam. “Siapapun yang tidak menuruti perintahku, akan dijatuhi hukuman berat!”, ujar Bandung Bondowoso pada rakyatnya. Bandung Bondowoso adalah seorang yang sakti dan mempunyai pasukan jin. Tidak berapa lama berkuasa, Bandung Bondowoso suka mengamati gerak-gerik Loro Jonggrang, putri Raja Prambanan yang cantik jelita. “Cantik nian putri itu. Aku ingin dia menjadi permaisuriku,” pikir Bandung Bondowoso.</Text>

						<Text style={[Style.Atas, Style.textPutihKecil]}>Esok harinya, Bondowoso mendekati Roro Jonggrang. “Kamu cantik sekali, maukah kau menjadi permaisuriku ?”, Tanya Bandung Bondowoso kepada Roro Jonggrang. Roro Jonggrang tersentak, mendengar pertanyaan Bondowoso. “Laki-laki ini lancang sekali, belum kenal denganku langsung menginginkanku menjadi permaisurinya”, ujar Loro Jongrang dalam hati. “Apa yang harus aku lakukan ?”. Roro Jonggrang menjadi kebingungan. Pikirannya berputar-putar. Jika ia menolak, maka Bandung Bondowoso akan marah besar dan membahayakan keluarganya serta rakyat Prambanan. Untuk mengiyakannya pun tidak mungkin, karena Loro Jonggrang memang tidak suka dengan Bandung Bondowoso.</Text>
						<Image 
							source={require('./../img/roro4.jpg')}
							style={Style.logoJudul}
						/>
						<Text style={[Style.Atas, Style.textPutihKecil]}>“Bagaimana, Roro Jonggrang ?” desak Bondowoso. Akhirnya Roro Jonggrang mendapatkan ide. “Saya bersedia menjadi istri Tuan, tetapi ada syaratnya,” Katanya. “Apa syaratnya? Ingin harta yang berlimpah? Atau Istana yang megah?”. “Bukan itu, tuanku, kata Roro Jonggrang. Saya minta dibuatkan candi, jumlahnya harus seribu buah. “Seribu buah?” teriak Bondowoso. “Ya, dan candi itu harus selesai dalam waktu semalam.” Bandung Bondowoso menatap Roro Jonggrang, bibirnya bergetar menahan amarah. Sejak saat itu Bandung Bondowoso berpikir bagaimana caranya membuat 1000 candi. Akhirnya ia bertanya kepada penasehatnya. “Saya percaya tuanku bias membuat candi tersebut dengan bantuan Jin!”, kata penasehat. “Ya, benar juga usulmu, siapkan peralatan yang kubutuhkan!”</Text>
						<Image 
							source={require('./../img/roro5.jpg')}
							style={Style.logoJudul}
						/>
						<Text style={[Style.Atas, Style.textPutihKecil]}>Setelah perlengkapan di siapkan. Bandung Bondowoso berdiri di depan altar batu. Kedua lengannya dibentangkan lebar-lebar. “Pasukan jin, Bantulah aku!” teriaknya dengan suara menggelegar. Tak lama kemudian, langit menjadi gelap. Angin menderu-deru. Sesaat kemudian, pasukan jin sudah mengerumuni Bandung Bondowoso. “Apa yang harus kami lakukan Tuan ?”, tanya pemimpin jin. “Bantu aku membangun seribu candi,” pinta Bandung Bondowoso. Para jin segera bergerak ke sana kemari, melaksanakan tugas masing-masing. Dalam waktu singkat bangunan candi sudah tersusun hampir mencapai seribu buah.</Text>

						<Text style={[Style.Atas, Style.textPutihKecil]}>Sementara itu, diam-diam Roro Jonggrang mengamati dari kejauhan. Ia cemas, mengetahui Bondowoso dibantu oleh pasukan jin. “Wah, bagaimana ini?”, ujar Roro Jonggrang dalam hati. Ia mencari akal. Para dayang kerajaan disuruhnya berkumpul dan ditugaskan mengumpulkan jerami. “Cepat bakar semua jerami itu!” perintah Roro Jonggrang. Sebagian dayang lainnya disuruhnya menumbuk lesung. Dung… dung…dung! Semburat warna merah memancar ke langit dengan diiringi suara hiruk pikuk, sehingga mirip seperti fajar yang menyingsing.</Text>
						<Image 
							source={require('./../img/roro6.jpg')}
							style={Style.logoJudul}
						/>
						
						<Text style={[Style.Atas, Style.textPutihKecil]}>Pasukan jin mengira fajar sudah menyingsing. “Wah, matahari akan terbit!” seru jin. “Kita harus segera pergi sebelum tubuh kita dihanguskan matahari,” sambung jin yang lain. Para jin tersebut berhamburan pergi meninggalkan tempat itu. Bandung Bondowoso sempat heran melihat kepanikan pasukan jin.</Text>


						<Image 
							source={require('./../img/roro7.jpg')}
							style={Style.logoJudul}
						/>
						<Text style={[Style.Atas, Style.textPutihKecil]}>Paginya, Bandung Bondowoso mengajak Roro Jonggrang ke tempat candi. “Candi yang kau minta sudah berdiri!”. Roro Jonggrang segera menghitung jumlah candi itu. Ternyata jumlahnya hanya 999 buah!. “Jumlahnya kurang satu!” seru Loro Jonggrang. “Berarti tuan telah gagal memenuhi syarat yang saya ajukan”. Bandung Bondowoso terkejut mengetahui kekurangan itu. Ia menjadi sangat murka. “Tidak mungkin…”, kata Bondowoso sambil menatap tajam pada Roro Jonggrang. “Kalau begitu kau saja yang melengkapinya!” katanya sambil mengarahkan jarinya pada Roro Jonggrang. Ajaib! Roro Jonggrang langsung berubah menjadi patung batu. Sampai saat ini candi-candi tersebut masih ada dan terletak di wilayah Prambanan, Jawa Tengah dan disebut Candi Roro Jonggrang.</Text>


					</View>
				</ScrollView>
			</View>
		)
	}
}