import React, { Component } from 'react';

import { View, Text, ScrollView, Image, TouchableOpacity, Dimensions, Alert} from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";

var Style = require('./../Style');

export default class Sangkuriang extends Component{

	render(){
		
		return(
			<View style={Style.bgPutih2}>
				<ScrollView>
					<View style={[Style.row, Style.center]}>

						<Image 
							source={require('./../img/sangkuriang.jpg')}
							style={Style.logoJudul}
						/>
						<View style={Style.garisTipis}/>
						<Text style={[Style.Atas, Style.textPutihKecil]}>Pada jaman dahulu, di Jawa Barat hiduplah seorang putri raja yang bernama Dayang Sumbi. Ia mempunyai seorang anak laki-laki yang bernama Sangkuriang. Anak tersebut sangat gemar berburu di dalam hutan. Setiap berburu, dia selalu ditemani oleh seekor anjing kesayangannya yang bernama Tumang. Tumang sebenarnya adalah titisan dewa, dan juga bapak kandung Sangkuriang, tetapi Sangkuriang tidak tahu hal itu dan ibunya memang sengaja merahasiakannya.</Text>
						
						<Image source={require('./../img/sangkuriang2.jpg')} style={Style.logoJudul} />

						<Text style={[Style.Atas, Style.textPutihKecil]}>Pada suatu hari, seperti biasanya Sangkuriang pergi ke hutan untuk berburu. Setelah sesampainya di hutan, Sangkuriang mulai mencari buruan. Dia melihat ada seekor burung yang sedang bertengger di dahan, lalu tanpa berpikir panjang Sangkuriang langsung menembaknya, dan tepat mengenai sasaran. Sangkuriang lalu memerintah Tumang untuk mengejar buruannya tadi, tetapi si Tumang diam saja dan tidak mau mengikuti perintah Sangkuriang. Karena sangat jengkel pada Tumang, maka Sangkuriang lalu mengusir Tumang dan tidak diijinkan pulang ke rumah bersamanya lagi.</Text>

						<Text style={[Style.Atas, Style.textPutihKecil]}>Sesampainya di rumah, Sangkuriang menceritakan kejadian tersebut kepada ibunya. Begitu mendengar cerita dari anaknya, Dayang Sumbi sangat marah. Diambilnya sendok nasi, dan dipukulkan ke kepala Sangkuriang. Karena merasa kecewa dengan perlakuan ibunya, maka Sangkuriang memutuskan untuk pergi mengembara, dan meninggalkan rumahnya.

Setelah kejadian itu, Dayang Sumbi sangat menyesali perbuatannya. Ia berdoa setiap hari, dan meminta agar suatu hari dapat bertemu dengan anaknya kembali. Karena kesungguhan dari doa Dayang Sumbi tersebut, maka Dewa memberinya sebuah hadiah berupa kecantikan abadi dan usia muda selamanya.</Text>
						<Image 
							source={require('./../img/sangkuriang3.jpg')}
							style={Style.logoJudul}
						/>
						<Text style={[Style.Atas, Style.textPutihKecil]}>Setelah bertahun-tahun lamanya Sangkuriang mengembara, akhirnya ia berniat untuk pulang ke kampung halamannya. Sesampainya di sana, dia sangat terkejut sekali, karena kampung halamannya sudah berubah total. Rasa senang Sangkuriang tersebut bertambah ketika saat di tengah jalan bertemu dengan seorang wanita yang sangat cantik jelita, yang tidak lain adalah Dayang Sumbi. Karena terpesona dengan kecantikan wanita tersebut, maka Sangkuriang langsung melamarnya. Akhirnya lamaran Sangkuriang diterima oleh Dayang Sumbi, dan sepakat akan menikah di waktu dekat. Pada suatu hari, Sangkuriang meminta ijin calon istrinya untuk berburu di hatan. Sebelum berangkat, ia meminta Dayang Sumbi untuk mengencangkan dan merapikan ikat kapalanya. Alangkah terkejutnya Dayang Sumbi, karena pada saat dia merapikan ikat kepala Sangkuriang, Ia melihat ada bekas luka. Bekas luka tersebut mirip dengan bekas luka anaknya. Setelah bertanya kepada Sangkuriang tentang penyebab lukanya itu, Dayang Sumbi bertambah tekejut, karena ternyata benar bahwa calon suaminya tersebut adalah anaknya sendiri.</Text>
						<Image 
							source={require('./../img/sangkuriang4.jpg')}
							style={Style.logoJudul}
						/>
						<Text style={[Style.Atas, Style.textPutihKecil]}>Dayang Sumbi sangat bingung sekali, karena dia tidak mungkin menikah dengan anaknya sendiri. Setelah Sangkuriang pulang berburu, Dayang Sumbi mencoba berbicara kepada Sangkuriang, supaya Sangkuriang membatalkan rencana pernikahan mereka. Permintaan Dayang Sumbi tersebut tidak disetujui Sangkuriang, dan hanya dianggap angin lalu saja.

Setiap hari Dayang Sumbi berpikir bagaimana cara agar pernikahan mereka tidak pernah terjadi. Setelah berpikir keras, akhirnya Dayang Sumbi menemukan cara terbaik. Dia mengajukan dua buah syarat kepada Sangkuriang. Apabila Sangkuriang dapat memenuhi kedua syarat tersebut, maka Dayang Sumbi mau dijadikan istri, tetapi sebaliknya jika gagal maka pernikahan itu akan dibatalkan. Syarat yang pertama Dayang Sumbi ingin supaya sungai Citarum dibendung. Dan yang kedua adalah, meminta Sangkuriang untuk membuat sampan yang sangat besar untuk menyeberang sungai. Kedua syarat itu harus diselesai sebelum fajar menyingsing.</Text>

						<Text style={[Style.Atas, Style.textPutihKecil]}>Sangkuriang menyanggupi kedua permintaan Dayang Sumbi tersebut, dan berjanji akan menyelesaikannya sebelum fajar menyingsing. Dengan kesaktian yang dimilikinya, Sangkuriang lalu mengerahkan teman-temannya dari bangsa jin untuk membantu menyelesaikan tugasnya tersebut. Diam-diam, Dayang Sumbi mengintip hasil kerja dari Sangkuriang. Betapa terkejutnya dia, karena Sangkuriang hampir menyelesaiklan semua syarat yang diberikan Dayang Sumbi sebelum fajar.</Text>
						<Image 
							source={require('./../img/sangkuriang5.jpg')}
							style={Style.logoJudul}
						/>
						
						<Text style={[Style.Atas, Style.textPutihKecil]}>Dayang Sumbi lalu meminta bantuan masyarakat sekitar untuk menggelar kain sutera berwarna merah di sebelah timur kota. Ketika melihat warna memerah di timur kota, Sangkuriang mengira kalau hari sudah menjelang pagi. Sangkuriang langsung menghentikan pekerjaannya dan merasa tidak dapat memenuhi syarat yang telah diajukan oleh Dayang Sumbi.</Text>

						<Image 
							source={require('./../img/sangkuriang6.jpg')}
							style={Style.logoJudul}
						/>

						<Text style={[Style.Atas, Style.textPutihKecil]}>Dengan rasa jengkel dan kecewa, Sangkuriang lalu menjebol bendungan yang telah dibuatnya sendiri. Karena jebolnya bendungan itu, maka terjadilah banjir dan seluruh kota terendam air. Sangkuriang juga menendang sampan besar yang telah dibuatnya. Sampan itu melayang dan jatuh tertelungkup, lalu menjadi sebuah gunung yang bernama Tangkuban Perahu.</Text>


					</View>
				</ScrollView>
			</View>
		)
	}
}