import React, { Component } from 'react';

import { View, Text, ScrollView, Image, TouchableOpacity, Dimensions, Alert} from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";

var Style = require('./../Style');

export default class Buaya extends Component{

	render(){
		
		return(
			<View style={Style.bgPutih2}>
				<ScrollView>
					<View style={[Style.row, Style.center]}>

						<Image 
							source={require('./../img/buaya4.jpg')}
							style={Style.logoJudul}
						/>
						<View style={Style.garisTipis}/>
						<Text style={[Style.Atas, Style.textPutihKecil]}>
							Di pinggiran sungai ada seekor buaya yang sedang kelaparan, sudah tiga hari Buaya itu belum makan perutnya terasa la sekali mau tidak mau hari ini dia harus makan sebab kalau tidak bisa-bisa ia akan mati kelaparan. Buaya itu segera masuk ke dalam Sungai ia berenang perlahan-lahan menyusuri sungai mencari mangsa.

Buaya melihat seekor bebek yang juga sedang berenang di sungai, Bebek tahu dia sedang diawasi oleh Buaya, dia segera menepi. Melihat mangsanya akan kabur Buaya segera mengejar dan akhirnya Bebekpun tertangkap.
						</Text>
						
						<Image source={require('./../img/buaya2.jpg')} style={Style.logoJudul} />

						<Text style={[Style.Atas, Style.textPutihKecil]}>
							Ampun Buaya, tolong jangan mangsa aku, dagingku sedikit, kenapa kamu tidak memang sa kambing saja di dalam hutan,” ucapnya seraya menagis ketakutan

“Baik, sekarang kau antar aku ke tempat persembunyian Kambing itu,” perintah buaya dengan menunjukkan taring yang sangat tajam.

Berada tidak jauh dari tempat itu ada lapangan hijau tempat Kambing mencari makan, dan benar saja di sana ada banyak Kambing yang sedang lahap memakan rumput.

“Pergi sanah, aku mau memangsa Kambing saja,” Bebek yang merasa senang, kemudian berlari dengan kecepatan penuh.
						</Text>

						<Text style={[Style.Atas, Style.textPutihKecil]}>
							Setelah mengintai beberapa lama, akhirnya Buaya mendapatkan satu ekor anak Kambing yang siap dia santap. “Tolong, jangan makan aku, dagingku tidak banyak, aku masih kecil, kenpa kamu tidak makan gajah saja yang dagingnya lebih banyak, aku bisa mengantarkan kamu ke sana”.
						</Text>
						
						<Text style={[Style.Atas, Style.textPutihKecil]}>
							“Baik, segera antarkan aku ke sana!” Anak Kambing itu mengajak buaya ke tepi danau yang luas, di sana ada anak Gajah yang besar. Buaya langsung mengejar dan menggigit kaki anak Gajah itu. Walau besar, tapi kulit Gajah itu sangat tebal, jadi tidak bisa melukainya.
						</Text>
						<Image 
							source={require('./../img/buaya3.jpg')}
							style={Style.logoJudul}
						/>
						<Text style={[Style.Atas, Style.textPutihKecil]}>
							Anak Gajah itu berteriak meminta tolong kepada ibunya. Buaya terus saja berusaha menjatuhkan anak Gajah itu, tapi sayang tetap tidak bisa. Mendengar teriakan anaknya, sekumpulan Gajah mendatangi dan menginjak Buaya itu sampai tidak bisa bernafas. Buaya itu tidak bisa melawan, karena ukuran ibu Gajah itu sangat besar, ditambah dia juga lemas karena belum makan. Buaya itu kehabisan tenaga dan mati.
						</Text>

					</View>
				</ScrollView>
			</View>
		)
	}
}